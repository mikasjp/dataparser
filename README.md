# Data Parsing Example

## 1. Overview

This is an example of a processing application that integrates data from two systems. It is possible to run applications on different sets of data.
There should be two input files:

* The first file should be in CSV format with the following column structure:

```csv
"employee_type","id","first_name","last_name","email","gender","end_date","ext1"
```

* The second file should be in XML format and should have the following structure:

```csv
<?xml version="1.0" encoding="utf-8"?>
<Objects>
    <Object>
        <Property Name="id"></Property>
        <Property Name="username">JLOCK0</Property>
        <Property Name="phoneType">phone1</Property>
        <Property Name="value">346-209-9609</Property>
    </Object>
    <Object>
        <Property Name="id"></Property>
        <Property Name="username">JLOCK0</Property>
        <Property Name="phoneType">phone2</Property>
        <Property Name="value">(478) 8085400</Property>
    </Object>
</Objects>
```

## 2. Usage

Parameters:

* `no parameters` - uses default files (Data\users.csv and Data\phone.xml),
* `--help` - displays help,
* `csv_file xml_file` - uses files chosen by user

## 3. Output

There are several requirements that are be met for the output data:

* FullTime users have "id" attribute (GUID) which is a good candidate to match data between two systems, unfortunately in phones.xml it has different representation,
* Contractor users don't have "id", but it should be possible to match them using different approach,
* Exclude records that have "end_date" in the past,
* Convert all phone numbers to a uniform format "XXX XXX XXXX",
* Clean-up data - exclude values containing unexpected format - in such case empty string should be exported to the output.

The output is a text file in JSON format containing array of objects with the following attributes:

```csv
employee_type, id, first_name, last_name, email, login, gender, end_date, phone1, phone2, phone3, ext1
```

## 4. Used techniques

### 4.1 CSV parsing

Processing of a CSV file is based on a simple technique of dividing individual lines of text into parts separated by a delimiter. The only data that is verified is email (using regular expression) and date.
Each line is another data record which is saved as a new data record in the corresponding list of objects.

### 4.2 XML parsing

The XML file is processed by iterating over all nodes of the Objects object. The content of each of the iterated objects (data record) is further identified (according to the first attribute) as a data field.
Each field is processed in the following way:

* Id - It is checked if the field contains content. If so, the content is decoded using the base64 algorithm to the byte array, which is then processed into Guid.
* username - The field is not verified and is copied without changes.
* phoneType - The field is not verified and is copied without changes.
* value - The field is filtered in such a way that it contains only numbers. The parser then checks whether filtered data is 10 characters long. If so - the data is formatted according to the guidelines.

As in the previous case, the processed data is saved as a new data record in the corresponding list of objects.

### 4.3 Data integration

Data integration is done using Linq.
Phone numbers are assigned to individual users depending on the employee type:

* Telephone numbers of full-time employees are identified by a foreign key - field id (Guid).
* Telephone numbers of contractors are identified by a foreign key in the form of a login generated from email addresses.

Such integrated data is exported as a list of objects in JSON format in the following format:

`employee_type, id, first_name, last_name, email, login, gender, end_date, phone1, phone2, phone3, ext1`

## 5. TODO

* Better data mismatches reporting,
* Unit tests