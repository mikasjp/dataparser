﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataParsing
{
    public sealed class CsvDeserializer
    {
        string _file;

        public CsvDeserializer(string file)
        {
            if (!File.Exists(file))
                throw new FileNotFoundException();
            _file = file;
        }

        private User ParseRow(string row)
        {
            row = row.Substring(1, row.Length - 2);
            var elements = row.Split(new string[] { "\",\"" }, StringSplitOptions.None);
            if (elements.Length != 8)
                return null;
            else
            {
                // Id
                Guid Id;
                Guid.TryParse(elements[1], out Id);

                // Employee type
                string EmployeeType = elements[0];

                // First name
                string FirstName = elements[2];

                // Last name
                string LastName = elements[3];

                // Email
                string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
                string Email = Regex.IsMatch(elements[4], emailRegex, RegexOptions.IgnoreCase) ? elements[4] : String.Empty;

                // Gender
                string Gender = elements[5];

                // End date
                DateTime EndDate;
                DateTime.TryParse(elements[6], out EndDate);

                //Ext1
                string Ext1 = elements[7];

                return new User()
                {
                    Id = Id,
                    EmployeeType = EmployeeType,
                    FirstName = FirstName,
                    LastName = LastName,
                    Email = Email,
                    Gender = Gender,
                    EndDate = EndDate,
                    Ext1 = Ext1
                };
            }
        }

        public IEnumerable<User> GetRecords()
        {
            List<User> result = new List<User>();

            using (StreamReader r = new StreamReader(_file))
            {
                string row;
                bool isFirstRow = true;
                while ((row = r.ReadLine()) != null)
                {
                    if(isFirstRow)
                    {
                        isFirstRow = false;
                        continue;
                    }
                    User record = ParseRow(row);
                    if (record != null)
                        result.Add(record);
                }
            }

            return result;
        }
    }
}
