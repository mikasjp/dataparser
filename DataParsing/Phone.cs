﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DataParsing
{
    public class Phone
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string PhoneType { get; set; }
        public string Value { get; set; }
    }
}
