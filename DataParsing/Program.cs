﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DataParsing
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1 && args[0] == "--help")
            {
                Console.WriteLine("Usage: converter [csv_file xml_file]");
                Console.WriteLine(@"Defaults: csv_file=Data\users.csv, xml_file=Data\phone.xml");
                return;
            }

            string csvFile = args.Length == 2 ? args[1] : "Data\\users.csv";
            string xmlFile = args.Length == 2 ? args[2] : "Data\\phone.xml";

            Console.WriteLine($"Selected files:\n{new FileInfo(csvFile).FullName}\n{new FileInfo(xmlFile).FullName}");

            try
            {
                Console.WriteLine("Processing CSV file...");
                CsvDeserializer csv = new CsvDeserializer(csvFile);
                var users = csv.GetRecords();

                Console.WriteLine("Processing XML file...");
                XmlDeserializer xml = new XmlDeserializer(xmlFile);
                var phones = xml.GetRecords();

                List<OutputElement> output = new List<OutputElement>();

                // Full time
                Console.WriteLine("Processing full-time employees...");
                foreach (User u in users.Where(x => x.EndDate > DateTime.Today && x.EmployeeType == "FullTime"))
                {
                    OutputElement o = new OutputElement()
                    {
                        employee_type = u.EmployeeType,
                        id = u.Id.ToString(),
                        first_name = u.FirstName,
                        last_name = u.LastName,
                        email = u.Email,
                        login = (u.Email
                                 .Split('@')
                                 .FirstOrDefault()
                                 ?? String.Empty).ToUpper(),
                        gender = u.Gender,
                        end_date = u.EndDate.ToShortDateString(),

                        phone1 = phones.SingleOrDefault(x => x.Id == u.Id && x.PhoneType == "phone1")?.Value ?? String.Empty,
                        phone2 = phones.SingleOrDefault(x => x.Id == u.Id && x.PhoneType == "phone2")?.Value ?? String.Empty,
                        phone3 = phones.SingleOrDefault(x => x.Id == u.Id && x.PhoneType == "phone3")?.Value ?? String.Empty,

                        ext1 = u.Ext1
                    };

                    output.Add(o);
                }

                // Contractors
                Console.WriteLine("Processing contractors...");
                foreach (User u in users.Where(x => x.EndDate > DateTime.Today && x.EmployeeType == "Contractor"))
                {
                    string login = u.Email
                                    .Split('@')
                                    .FirstOrDefault()?
                                    .ToUpper();

                    if (login == null)
                        Console.WriteLine($"Can't match phone numbers with Contractor: {u.FirstName} {u.LastName} [{u.Email}]");

                    OutputElement o = new OutputElement()
                    {
                        employee_type = u.EmployeeType,
                        id = String.Empty,
                        first_name = u.FirstName,
                        last_name = u.LastName,
                        email = u.Email,
                        login = login,
                        gender = u.Gender,
                        end_date = u.EndDate.ToShortDateString(),

                        phone1 = login == null ? String.Empty : phones.SingleOrDefault(x => x.Username == login && x.PhoneType == "phone1")?.Value ?? String.Empty,
                        phone2 = login == null ? String.Empty : phones.SingleOrDefault(x => x.Username == login && x.PhoneType == "phone2")?.Value ?? String.Empty,
                        phone3 = login == null ? String.Empty : phones.SingleOrDefault(x => x.Username == login && x.PhoneType == "phone3")?.Value ?? String.Empty,

                        ext1 = u.Ext1
                    };

                    output.Add(o);
                }

                // Export
                var outputJson = JsonConvert.SerializeObject(output.ToArray(), Formatting.Indented);

                Console.WriteLine("Saving output JSON file.");
                using (var writer = new StreamWriter("output.json"))
                {
                    writer.Flush();
                    writer.Write(outputJson);
                }

                Console.WriteLine("Done! Results exported to output.json file.");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return;
            }
        }
    }
}
