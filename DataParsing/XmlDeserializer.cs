﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DataParsing
{
    public class XmlDeserializer
    {
        string _file;
        public XmlDeserializer(string file)
        {
            if (!File.Exists(file))
                throw new FileNotFoundException();
            _file = file;
        }

        public IEnumerable<Phone> GetRecords()
        {
            List<Phone> result = new List<Phone>();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(File.ReadAllText(_file));
            var phones = doc.DocumentElement.SelectNodes("/Objects/Object");

            foreach (XmlNode p in phones)
            {
                Phone phone = new Phone();
                foreach (XmlNode n in p.ChildNodes)
                {
                    string name = n.Attributes[0].Value;
                    if (name == "id")
                    {
                        if (n.InnerText.Length > 0)
                        {
                            byte[] b = Convert.FromBase64String(n.InnerText);
                            if (b.Length > 0)
                                phone.Id = new Guid(b);
                        }
                    }

                    if (name == "username")
                    {
                        phone.Username = n.InnerText;
                    }

                    if (name == "phoneType")
                    {
                        phone.PhoneType = n.InnerText;
                    }

                    if (name == "value")
                    {
                        string cleaned = (new Regex(@"[^\d]+")).Replace(n.InnerText, String.Empty);
                        if (cleaned.Length == 10)
                        {
                            phone.Value = $"{cleaned.Substring(0, 3)} {cleaned.Substring(3, 3)} {cleaned.Substring(6, 4)}";
                        }
                    }

                }
                result.Add(phone);
            }

            return result;
        }
    }
}
